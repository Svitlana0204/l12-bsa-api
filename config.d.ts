declare var appConfig: {
    envName: string;
    baseUrl: string;
    swaggerUrl: string;
    users: Record<string, any>;
};
