import "chai";
import "chai-json-schema";

declare global {
    namespace Chai {
        interface Assertion {
            normalTime: Assertion;
            status(status: number): Assertion;
        }

        interface LanguageChain {
            normalTime: Assertion;
            status(status: number): Assertion;
        }
    }
}
