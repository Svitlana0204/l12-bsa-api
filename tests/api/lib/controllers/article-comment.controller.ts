import CreateArticleComment from "../models/article-comments/create-article-comment.model";
import { ApiRequest } from "../request";

export class ArticleCommentController {
    createComment(data: CreateArticleComment) {
        return new ApiRequest()
            .method("POST")
            .url("article_comment")
            .body(data)
            .send();
    }

    getCommentsOf(articleId: string) {
        return new ApiRequest()
            .method("GET")
            .url(`article_comment/of/${articleId}`)
            .send();
    }
}
