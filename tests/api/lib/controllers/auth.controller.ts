import LoginData from "../models/auth/login-data.model";
import { ApiRequest } from "../request";

export class AuthController {
    login(data: LoginData) {
        return new ApiRequest()
            .method("POST")
            .url("auth/login")
            .body(data)
            .send();
    }
}
