import { ApiRequest } from "../request";

type FavoriteType =
    | "ARTICLE"
    | "AUTHOR"
    | "COURSE"
    | "LECTURE"
    | "PATH"
    | "PERSONAL_GOAL_COMPLETION"
    | "SCHOOL";

export class FavoriteController {
    isFavorite(id: string, type: FavoriteType) {
        return new ApiRequest()
            .method("GET")
            .url("favorite")
            .searchParams({ id, type })
            .send();
    }

    changeFavorite(id: string, type: FavoriteType) {
        return new ApiRequest()
            .method("POST")
            .url("favorite/change")
            .searchParams({ id, type })
            .send();
    }
}
