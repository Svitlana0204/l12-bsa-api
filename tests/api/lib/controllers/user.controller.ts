import { ApiRequest } from "../request";

export class UserController {
    getCurrentUser() {
        return new ApiRequest().method("GET").url("user/me").send();
    }
}
