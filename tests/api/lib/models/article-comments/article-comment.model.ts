import UserShortInfo from "../user/user-short-info.model";

export default interface ArticleComment {
    id: string;
    text: string;
    sourceId: string;
    articleId: string;
    createdAt: string;
    user: UserShortInfo;
}
