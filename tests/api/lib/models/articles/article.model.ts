export default interface Article {
    id: string;
    name: string;
    text: string;
    image: string;
}
