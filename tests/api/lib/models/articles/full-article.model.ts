import AuthorWithArticles from "../author/author-with-articles.model";

export default interface FullArticle {
    author: AuthorWithArticles;
    favourite: boolean;
    id: string | null;
    image: string;
    name: string;
    text: string;
}
