export default interface AuthorSettings {
    avatar: string | null;
    biography: string;
    company: string;
    firstName: string;
    id: string;
    job: string;
    lastName: string;
    location: string;
    twitter: string;
    website: string;
}
