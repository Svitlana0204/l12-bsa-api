import Tag from "../tags/tag.model";

export default interface Course {
    id: string;
    name: string;
    level: string;
    author: string;
    tags: Tag[];
    imageSrc: string;
    duration: number;
    positiveReactions: number;
    allReactions: number;
    updatedAt: string;
    lectures: number;
    description: string;
    members: number;
}
