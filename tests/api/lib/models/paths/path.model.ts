export default interface Path {
    courses: number;
    duration: number;
    id: string;
    logoSrc: string;
    name: string;
}
