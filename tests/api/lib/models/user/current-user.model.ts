export default interface CurrentUser {
    avatar: string | null;
    email: string;
    emailVerified: boolean;
    id: string;
    nickname: string | null;
    role: {
        name: "AUTHOR" | "USER";
    };
}
