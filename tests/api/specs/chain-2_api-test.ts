import { expect } from "chai";
import { v4 } from "uuid";
import { AuthController } from "../lib/controllers/auth.controller";
import { FavoriteController } from "../lib/controllers/favorite.controller";
import { PathController } from "../lib/controllers/path.controller";
import { UserController } from "../lib/controllers/user.controller";
import Path from "../lib/models/paths/path.model";
import { ApiRequest } from "../lib/request";
import schemas from "./data/schemas_testData.json";

const auth = new AuthController();
const user = new UserController();
const path = new PathController();
const favorite = new FavoriteController();

describe("Chain 2", () => {
    let randomPath: Path;
    let invalidPathId: string;

    const loginDataSet = [
        { ...appConfig.users.student, valid: true },
        {
            email: "jevif54135@altpano.com",
            password: "123456789",
        },
        {
            email: "jevif54135@altpano.com",
            password: "    ",
        },
        {
            email: "jevif54135@altpano.com",
            password: "1234",
        },
        {
            email: "jevif54135@altpano.com",
            password: "jevif54135@altpano.com",
        },
        {
            email: "jevif54135 @altpano.com",
            password: "123456789a",
        },
    ];

    loginDataSet.forEach((loginData) => {
        const name = loginData.valid
            ? "should log in"
            : `should not log in with invalid credentials: ${loginData.email} + ${loginData.password}`;

        const func = loginData.valid ? before : it;

        func(name, async () => {
            const response = await auth.login(loginData);

            expect(response).to.have.status(loginData.valid ? 200 : 401);
            expect(response).to.have.normalTime;

            if (loginData.valid) {
                expect(response.body).to.be.jsonSchema(
                    schemas.schema_loginResponse
                );

                ApiRequest.authToken = response.body.accessToken;
            }
        });
    });

    it("should get current user", async () => {
        const response = await user.getCurrentUser();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_currentUser);

        expect(response.body).to.deep.include({
            email: appConfig.users.student.email,
            emailVerified: true,
            role: {
                name: "USER",
            },
        });
    });

    it("should get all paths", async () => {
        const response = await path.getAllPaths();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_arrayOfPaths);

        const existingPathIds = response.body.map((path) => path.id);

        randomPath =
            response.body[Math.floor(Math.random() * response.body.length)];

        do {
            invalidPathId = v4();
        } while (existingPathIds.includes(invalidPathId));
    });

    it("should add path to favorites", async () => {
        const response = await favorite.changeFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.true;
    });

    it("should check if path is favorite", async () => {
        const response = await favorite.isFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.true;
    });

    it("should remove path from favorites", async () => {
        const response = await favorite.changeFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.false;
    });

    it("should check if path is favorite", async () => {
        const response = await favorite.isFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.false;
    });
});
