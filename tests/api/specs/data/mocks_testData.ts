import { faker } from "@faker-js/faker";
import CreateArticleComment from "tests/api/lib/models/article-comments/create-article-comment.model";
import CreateArticle from "tests/api/lib/models/articles/create-article.model";
import AuthorSettings from "tests/api/lib/models/author/settings.model";

export default class Mocks {
    static getAuthorSettings(id: string): AuthorSettings {
        return {
            id,
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            avatar: faker.image.avatar(),
            biography: faker.lorem.paragraphs(7),
            company: faker.company.companyName(),
            job: faker.name.jobTitle(),
            location: faker.address.country(),
            twitter: "https://twitter.com/" + faker.internet.userName(),
            website: faker.internet.url(),
        };
    }

    static getCreateArticleData(): CreateArticle {
        return {
            name: faker.lorem.words(1),
            text: faker.lorem.paragraphs(7),
            image: faker.image.technics(),
        };
    }

    static getCreateArticleCommentData(
        articleId: string
    ): CreateArticleComment {
        return {
            articleId,
            text: faker.lorem.sentences(2),
        };
    }
}
