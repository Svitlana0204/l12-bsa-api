import chai from "chai";
import ChaiHttpConstants from "./constants";

export default function chaiHttp(
    chai: Chai.ChaiStatic,
    _utils: Chai.ChaiUtils
) {
    chai.Assertion.addMethod("status", function (status: number) {
        const response = this._obj;

        this.assert(
            response.statusCode === status,
            "Expected response to have status #{exp} but got #{act}",
            "Expected response to have status #{exp} but got different",
            status,
            response.statusCode
        );
    });

    chai.Assertion.addProperty("normalTime", function () {
        const response = this._obj;

        this.assert(
            response.timings.phases.total <= ChaiHttpConstants.MaxExecutionTime,
            "Expected response to have execution time less or equal to #{exp} but got #{act}",
            "Expected response to have execution time less or equal to #{exp} but got different",
            ChaiHttpConstants.MaxExecutionTime,
            response.timings.phases.total
        );
    });
}
