import chai from "chai";
import chaiJsonSchema from "chai-json-schema";
import chaiHttp from "./http";

chai.use(chaiJsonSchema);
chai.use(chaiHttp);
